FROM node:8.10.0-alpine as build-stage

COPY ./graphql-voyager /app
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

RUN set -x \
    && yarn install \
    && npm run build:demo

FROM nginx:1.15
COPY --from=build-stage /app/demo-dist/ /usr/share/nginx/html
