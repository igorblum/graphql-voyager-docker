```
git clone --recursive https://bitbucket.org/igorblum/graphql-voyager-docker.git

docker build -t graphql-voyager .

docker run --rm -p 9090:80 graphql-voyager

```

Open [http://localhost:9090](http://localhost:9090)